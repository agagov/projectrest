Feature: Request to todo.ly

  Scenario: User creation on the platform
    Given The user is found in the path to create user
    Then The user completes the form with the requested data

  Scenario: Creating a user with an existing email address
    Given The user is found in the path to create user
    Then The user completes the form with an existing email address

  Scenario: Creating a user with an invalid email address format
    Given The user is found in the path to create user
    Then The user completes the form with an invalid email address format

  Scenario: Verify if the user is authenticated
    Given The user is in the path to verify its authenticity
    When Enter the username and password of the user to be verified
    Then A message is displayed indicating whether it is true or false

  Scenario: Get the user token
    Given The user is in the path to verify its authenticity
    When Enter the username and password of the user to get the token
    Then A message is displayed indicating the token of the user

  Scenario: Get user information
    Given The user is in the path to verify its authenticity
    When Enter the username and password of the user to get information
    Then All the information of user is displayed

  Scenario: Update a user's email
    Given The user is in the path to verify its authenticity
    When Enter the new email to update
    Then All updated information is displayed